package br.com.wos.tecnologia.amqp.sender.controller;

import br.com.wos.tecnologia.amqp.core.model.Person;
import br.com.wos.tecnologia.amqp.sender.component.PersonQueueSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wesleysantos in 08/04/19
 */
@RestController
@RequestMapping(value = "/persons")
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OrderController {

    private final PersonQueueSender orderQueueSender;

    @PostMapping
    public void send(@RequestBody String order) {
        orderQueueSender.send(order);
    }

    @PostMapping(value = "/object")
    public void sendPerson(@RequestBody Person person) {
        orderQueueSender.send(person);
    }

}
