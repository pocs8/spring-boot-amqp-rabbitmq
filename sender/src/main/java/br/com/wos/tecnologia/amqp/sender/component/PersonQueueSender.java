package br.com.wos.tecnologia.amqp.sender.component;

import br.com.wos.tecnologia.amqp.core.model.Person;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by wesleysantos in 08/04/19
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PersonQueueSender {

    private final RabbitTemplate rabbitTemplate;

    private final Queue queue;

    public void send(String person) {
        rabbitTemplate.convertAndSend(this.queue.getName(), person);
    }

    public void send(Person person) {
        rabbitTemplate.convertAndSend(this.queue.getName(), person);
    }


}
