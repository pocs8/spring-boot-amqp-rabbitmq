package br.com.wos.tecnologia.amqp.sender.consumer.component;

import br.com.wos.tecnologia.amqp.core.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * Created by wesleysantos in 08/04/19
 */
@Component
@Slf4j
public class PersonConsumer {

    @RabbitListener(queues = {"${queue.person.name}"})
    public void receive(@Payload String order) {
        log.info("Person: " + order);
    }

    @RabbitListener(queues = {"${queue.person.name}"})
    public void receive(@Payload Person person) {
        log.info("Person: " + person.toString());
    }
}
